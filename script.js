/*variaveis iniciais*/
const urlAPILobos = "http://lobinhos.herokuapp.com/wolves"
var lobosAdocao = []
var lobosAdotados = []

/*funcoes */

function pegaLobos(adotado) {

    adotado = adotado || false;
    let fetchConfig = {
        method: 'GET'
    }

    let urlAdotados = urlAPILobos + '/adopted'

    fetch(urlAdotados, fetchConfig)
        .then((resposta) => resposta.json()
            .then((resp) => {
                for (lobo of resp) {
                    lobosAdotados.push(lobo)
                }
                console.log(lobosAdotados)
            })
            .catch((erro) => { console.log(erro)}))
        .catch((erro) => {console.log(erro)})

    fetch(urlAPILobos, fetchConfig)
        .then((resposta) => resposta.json()
            .then((resp) => {
                for (lobo of resp) {
                    lobosAdocao.push(lobo)
                }
            })
            .catch((erro) => { console.log(erro)}))
        .catch((erro) => {console.log(erro)})
}


function validaDados(nome, idade, foto, descricao) {
    if (nome.length < 4 || nome.length > 60) {
        alert('Nome deve ter entre 4 e 60 caracteres.')
        return false
    }
    else {

        idade = parseInt(idade)
        if (idade < 0 || idade > 100) {
            alert('Idade deve ser entre 0 e 100.')
            return false
        }

        else {
            if (foto.length <= 0) {
                alert('Campo de imagem vazio.')
                return false
            }

            else {
                if (descricao.length < 5 || descricao.length > 255) {
                    alert('A descrição deve ter entre 5 e 255 caracteres')
                    return false
                }
                else {
                    return true 
                }
            }
        }
    }
}

function enviaLobo() {

    let nome = document.querySelector('#nomelobin').value
    let idade = document.querySelector('#idadelobin').value
    let foto = document.querySelector('#fotolobin').value
    let descricao = document.querySelector('#descricaolobin').value

    console.log(nome)
    console.log(idade)
    console.log(foto)
    console.log(descricao)

    if (validaDados(nome, idade, foto, descricao)) {

        let fetchBody = {
            "wolf": {
                "name": nome,
                "age": parseInt(idade),
                "image_url": foto,
                "description": descricao
            }
        }
    
        let fetchConfig = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(fetchBody)
        }
        fetch(urlAPILobos, fetchConfig)
            .then(resposta => resposta.json()
                .then(resp => { console.log(resp) })
                .catch(erro => { console.log(erro) }))
            .catch(erro => {console.log(erro)})
    }
    else {
        return
    }
}



function adotaLobo() {

    let nomedono = document.querySelector('#name').value
    let idadedono = document.querySelector('#age').value
    let emaildono = document.querySelector('#email').value
    let idlobo = document.querySelector('#idlobo').textContent

    idlobo = idlobo.split(':')[1]

    console.log(nomedono)
    console.log(idadedono)
    console.log(emaildono)
    console.log(idlobo)

    let urllobin = urlAPILobos + '/' + idlobo
        
    let fetchBody = {
        "wolf": {
            "adopter_name": nomedono,
            "adopter_age": idadedono,
            "adopter_email": emaildono
        }
    }

    let fetchConfig = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(fetchBody)
    }

    fetch(urllobin, fetchConfig)
        .then(resposta => resposta.json()
        .then(resp => { console.log(resp) })
        .catch(erro => { console.log(erro) }))
    .catch(erro => {console.log(erro)})
}

function apagaLobo() {

    let url = window.location.href
    console.log(url)
    let idlobo = url.split('?')[1]
    let urllobin = urlAPILobos + '/' + idlobo

    let fetchConfig = {
        method: 'DELETE'
    }
    fetch(urllobin, fetchConfig)
        .then(resposta => resposta.json()
        .then(resp => { console.log(resp) })
        .catch(erro => { console.log(erro) }))
        .catch(erro => { console.log(erro) })
    
    window.location.href = 'listalobos.html'
}



    

function mostraLobinhosIndex() {
   

    let random1 = getRandomInt(0, lobosAdocao.length)
    let random2 = getRandomInt(0, lobosAdocao.length)
    let divMaeEsq = document.querySelector('.geralEsq')
    let divMaeDir = document.querySelector('.geralDir')

    divMaeEsq.innerHTML = '<div class="ladoEsq"><div class="imgEsq"><img src="' + lobosAdocao[random1].image_url + '"></div></div><div class="infoEsq"><h4 class="titEsq">' + lobosAdocao[random1].name + '</h4><h5 class="idadeEsq">Idade: ' + lobosAdocao[random1].age + ' anos </h5><p class="fraseEsq">' + lobosAdocao[random1].description + '</p></div>'
    
    divMaeDir.innerHTML = '<div class="infoDir"><h4 class="titDir">'+lobosAdocao[random2].name+'</h4><h5 class="idadeDir">Idade: '+lobosAdocao[random2].age+' anos</h5><p class="fraseDir">'+lobosAdocao[random2].description+'</p></div><div class="ladoDir"><div class="imgDir"><img src="'+lobosAdocao[random2].image_url+'"></div></div>'    
    
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

function mostraLobinhos() {

    let checkbox = document.querySelector('#lobinhosadotados')
    let carregaLobos = lobosAdocao
    if (checkbox.checked) {
        carregaLobos = lobosAdotados
        var estilolobo = {
            botao: 'id=adotado type="button">ADOTADO</button>'
        }
    }
    else {
        var estilolobo = {
        botao: 'id=adotar type="button"><a href="showlobo.html?'+lobo.id+'" onclick="pegaIdLobin(this.id))">ADOTAR</a></button>'
    }
    }

    let pesquisa = document.querySelector('.pesquisa')
    console.log(lobosAdocao)
    let divMae = document.querySelector('.lobo')
    divMae.innerHTML = ''
    let cont = 0
    


    for (lobo of carregaLobos) {

        if (checkbox.checked) {
            carregaLobos = lobosAdotados
            var estilolobo = {
                botao: 'id=adotado type="button">ADOTADO</button>'
            }
        }
        else {
            var estilolobo = {
            botao: 'id=adotar type="button"><a href="showlobo.html?'+lobo.id+'" onclick="pegaIdLobin(this.id))">ADOTAR</a></button>'
        }
        }
        cont++
        if (pesquisa.value != '' && !(lobo.name.includes(pesquisa.value))) {
            continue
        }
        else {
            if (cont % 2 == 0) {
                divMae.innerHTML += '<div class="geralEsq"><div class="ladoEsq"><div class="imgEsq"><img src="'+lobo.image_url+'"></div></div><div class="infoEsq"><div class="loboEsq"><div><h4 id="normal">Nome do lobo: '+lobo.name+'</h4><h5 id="normal">Idade: '+lobo.age+' anos</h5></div><button class="btnEsq" '+estilolobo.botao+'</div><p class="fraseEsq">'+lobo.description+'</p></div></div><br><br>'
            }
            else {
                divMae.innerHTML += '<div class="geralDir"><div class="infoDir"><div class="loboDir"><button class="btnDir" '+estilolobo.botao+'<div><h4 id="reverse">Nome do lobo: '+lobo.name+'</h4><h5 id="reverse">Idade: '+lobo.age+' anos</h5></div></div><p class="fraseDir">'+lobo.description+'</p></div><div class="ladoDir"><div class="imgDir"><img src="'+lobo.image_url+'"></div></div></div><br><br>'
            }
        }
            
    }
    
}


function adotandoLobin() {
    let url = window.location.href
    console.log(url)
    let id = url.split('?')[1]

    let divMae = document.querySelector('.topoAdotar')
    for (lobo of lobosAdocao) {
        console.log(id)
        console.log(lobo.id)
        if (lobo.id == id) {
            var lobin = lobo
        }
    }

    divMae.innerHTML = '<picture><img src="'+lobin.image_url+'"></picture><div class="titAdotar"><h1>Adote a(o) '+lobin.name+'</h1><h2 id="idlobo">ID:'+lobin.id+'</h2></div>'
}

function mostraLobo() {
    let url = window.location.href
    console.log(url)
    let id = url.split('?')[1]

    let divMae = document.querySelector('.lobin')
    for (lobo of lobosAdocao) {
        console.log(id)
        console.log(lobo.id)
        if (lobo.id == id) {
            var lobin = lobo
        }
    }

    divMae.innerHTML = '<div class="titLobin"><h2>'+lobin.name+'</h2></div><div class="geralEsq"><div class="ladoEsq"><div class="imgEsq"><img src="'+lobin.image_url+'"></div></div><div class="infoEsq"><p class="fraseShow">'+lobin.description+'</p></div></div><div class="botoes"><button class="adotaLobin" type="button"><a href="adotarlobo.html?'+lobin.id+'">ADOTAR</a></button><button class="apagaLobin" type="button" onclick="apagaLobo()">EXCLUIR</button></div></div><br><br></br>'
        
}

